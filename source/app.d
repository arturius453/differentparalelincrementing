import std.datetime;
import imagefmt;
import std.stdio;
import std.parallelism;

struct Point8{
ubyte r,g,b;
}

void main(){
	IFImage a = read_image("a.bmp", 3);     // convert to rgb
	if (a.e) {
	    writefln("*** load error: %s\n", IF_ERROR[a.e].ptr);
	    return;
	}
	scope(exit) a.free();
	auto arr=cast(Point8[])a.buf8;
	int counterW=0,counterB=0;
    auto start=Clock.currTime();
	foreach(ref p;arr.parallel){
		if (p==Point8(255,255,255)){
				synchronized{
				counterW++;
				}
		}
		if (p==Point8(0,0,0)){
				synchronized{
				counterB++;
				}
		}
		p=Point8(255-p.r,255-p.g,255-p.b);
	}
    writeln(Clock.currTime()-start,"-час виконнная ");
	[counterB,counterW].writeln(" blacks and whites");
	write_image("b.bmp",a.w,a.h,cast(ubyte[])arr);
}
